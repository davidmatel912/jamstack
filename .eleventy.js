module.exports = function(config) {

  config.addPassthroughCopy("src/js");

  config.setBrowserSyncConfig({
    https: {
      key: './server.key',
      cert: './server.crt'
    }
  });

  return  {
    dir: {
      input: "src",
      output: "dist",
      data: "_data"
    }
  };

};